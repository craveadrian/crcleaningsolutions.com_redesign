<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop">
				<div class="row">
					<div class="hdTopLeft col-6 inbMid">
						<p>EMAIL: <span class="email inbBot"><?php $this->info(["email","mailto"]); ?></span></p>
					</div>
					<div class="hdTopRight col-6 inbMid">
						<p>PHONE: <span class="phone inbMid"><?php $this->info(["phone","tel"]); ?></span> </p>
					</div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<div class="hdBotLeft col-6 inbTop">
						<a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="CR Cleaning Solutions Logo"> </a>
					</div>
					<div class="hdBotRight col-6 inbTop">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">REVIEWS</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="bnrTop">
				<div class="row">
					<p>The Superior Choice <br> for Cleaning Services</p>
					<img src="public/images/common/img1.png" alt="cleaning tools" class="img1">
					<img src="public/images/common/img2.png" alt="Badge" class="img2">
				</div>
			</div>
			<div class="bnrBot">
				<div class="row">
						<p>We will be happy to consult with you and offer you a free estimate for all of your office cleaning service needs. Whether you are a small business in  Cucamonga, CA  or a large corporation, we have experience providing janitorial office cleaning maintenance services to your type of organization.</p>
						<a href="<?php echo URL ?>contact#content" class="btn">Get A FREE Quote</a> <a href="<?php echo URL ?>contact#content" class="btn2">Book An Appointment</a>
					</div>
				</div>
			</div>
	<?php //endif; ?>
