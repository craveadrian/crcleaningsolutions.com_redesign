<div id="content">
	<div class="row">
		<h1>Services</h1>
    <p>If you're looking for a commercial cleaning company that will tailor to your needs, look no further than CR Cleaning Solutions. Service contracts are available as well as after hours cleaning.</p>
		<p>Environmentally friendly cleaning supplies.</p>
		<p>From office complexes and warehouses to factories and medical buildings, we do it all. Our quality and attention to detail is what has kept us in the industry for so long. Let us put our 15 years of experience to work for you.</p>
		<h2>Free estimates on all cleaning services:</h2>
		<ul>
			<li><p>Vacuuming</p></li>
			<li><p>Construction clean-up</p></li>
			<li><p>Warehouse cleaning</p></li>
			<li><p>Concrete floor polishing</p></li>
			<li><p>Window cleaning</p></li>
			<li><p>Carpet cleaning</p></li>
			<li><p>Odor removal</p></li>
		</ul>
	</div>
</div>
