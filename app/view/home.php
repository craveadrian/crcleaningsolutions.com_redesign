<div id="content">
	<div id="welcome-section">
		<div class="row">
			<div class="wlc-panel fr">
				<h3>Welcome To </h3>
				<h1>CR <span>Cleaning Solutions </span></h1>
				<h3>Experienced Commercial Cleaning Company</h3>
				<p>When you’re choosing a company to clean your office, quality and flexibility is a must.</p>
				<p>CR Cleaning Solutions will design a plan to meet your specific needs. Offering free estimates and weekly, bi-weekly, and daily cleanings, why choose anyone else?</p>
				<p>Our cleaning supplies are environmentally friendly and we’ve been cleaning office complexes, warehouses, factories, and medical buildings for over 15 years.</p>
				<a href="<?php echo URL ?>contact#content" class="btn2">Book An Appointment</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="service-section">
		<div class="row">
			<h1>Our Services</h1>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon1.png" alt="Service Icon"> </dt>
				<dd><p>Post-event <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon2.png" alt="Service Icon"> </dt>
				<dd><p>Kitchen <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon3.png" alt="Service Icon"> </dt>
				<dd><p>Move In/Move Out <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon4.png" alt="Service Icon"> </dt>
				<dd><p>Window <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon5.png" alt="Service Icon"> </dt>
				<dd><p>Post Construction <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon6.png" alt="Service Icon"> </dt>
				<dd><p>Janitorial <br> Services</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon7.png" alt="Service Icon"> </dt>
				<dd><p>Carpet <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon8.png" alt="Service Icon"> </dt>
				<dd><p>Office <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon9.png" alt="Service Icon"> </dt>
				<dd><p>Restroom <br> Cleaning</p></dd>
			</dl></a>
			<a href="<?php echo URL ?>service#content"><dl class="inbTop">
				<dt> <img src="public/images/content/our-service-icons/icon10.png" alt="Service Icon"> </dt>
				<dd><p>Garbage <br> Removal</p></dd>
			</dl></a>
			<p class="message">If you’re looking for a commercial cleaning company that will tailor to your needs,<br> look no further than CR Cleaning Solutions. Service contracts are available as well as after hours cleaning.</p>
		</div>
	</div>
	<div id="appointment-section">
		<div class="row">
			<div class="app-panel">
				<p>We promise quality, reliable cleaning service every time so our customers feel special. <span>We work hard to be the best.</span></p>
				<a href="<?php echo URL ?>contact#content" class="btn2">Book An Appointment</a>
			</div>
		</div>
	</div>
	<div id="about-gallery-section">
		<div class="row">
			<div class="about-home inbTop">
				<h1>About Us</h1>
				<p>Our co-staff is friendly,Professional and Available in 24 Hrs.We are locally Owned Operated,Licensed,Bonded,Insured and You can count on us on workers’ compensation.</p>
				<p>From office complexes and warehouses to factories and medical buildings, we do it all. Our quality and attention to detail is what has kept us in the industry for so long. Let us put our 15 years of experience to work for you.</p>
				<a href="<?php echo URL ?>about#content" class="btn2">Read More</a>
			</div>
			<div class="gallery-home inbTop">
				<h1> <span>Our</span> Gallery</h1>
				<img src="public/images/content/our-gallery-images/img1.jpg" alt="Gallery Image" class="imgGallery inbTop">
				<img src="public/images/content/our-gallery-images/img2.jpg" alt="Gallery Image" class="imgGallery inbTop">
				<img src="public/images/content/our-gallery-images/img3.jpg" alt="Gallery Image" class="imgGallery inbTop">
				<img src="public/images/content/our-gallery-images/img4.jpg" alt="Gallery Image" class="imgGallery inbTop">
				<img src="public/images/content/our-gallery-images/img5.jpg" alt="Gallery Image" class="imgGallery inbTop">
				<img src="public/images/content/our-gallery-images/img6.jpg" alt="Gallery Image" class="imgGallery inbTop">
			</div>
		</div>
	</div>
</div>
